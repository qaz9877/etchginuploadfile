package handler

import (
	"apigw/internal/service"
	"apigw/pkg/e"
	"apigw/pkg/res"
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

// DoSignupHandler : 处理注册post请求
func DoSignupHandler(c *gin.Context) {
	var userReq service.ReqSignup

	username := c.Request.FormValue("username")
	passwd := c.Request.FormValue("password")
	userReq = service.ReqSignup{
		Username: username,
		Password: passwd,
	}
	//fmt.Println(username, passwd)
	//fmt.Println("DoSignupHandler:")
	userService := c.Keys["user"].(service.UserServiceClient)

	resp, err := userService.Signup(context.Background(), &userReq)
	PanicIfUserError(err)

	if err != nil {
		log.Println(err.Error())
		c.Status(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": resp.Code,
		"msg":  resp.Message,
	})
}

// SignupHandler : 响应注册页面
func SignupHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/static/view/signup.html")
}

func SigninHandler(c *gin.Context) {
	c.Redirect(http.StatusFound, "/static/view/signin.html")
}

func DoSigninHandler(c *gin.Context) {

	var reqSignin service.ReqSignin
	//PanicIfUserError(c.Bind(&reqSignin))
	username := c.Request.FormValue("username")
	password := c.Request.FormValue("password")
	reqSignin = service.ReqSignin{
		Username: username,
		Password: password,
	}
	//encPasswd := util.Sha1([]byte(reqSignin.Password + e.PasswordSalt))
	userService := c.Keys["user"].(service.UserServiceClient)
	// 1. 校验用户名及密码
	resp, err := userService.Signin(context.Background(), &reqSignin)
	PanicIfUserError(err)

	// 登录成功，返回用户信息
	cliResp := res.RespMsg{
		Code: 200,
		Msg:  "登录成功",
		Data: struct {
			Location      string
			Username      string
			Token         string
			UploadEntry   string
			DownloadEntry string
		}{
			Location: "/static/view/home.html",
			Username: reqSignin.Username,
			Token:    resp.Token,
			// UploadEntry:   upEntryResp.Entry,
			// DownloadEntry: dlEntryResp.Entry,
			UploadEntry:   e.UploadLBHost,
			DownloadEntry: e.DownloadLBHost,
		},
	}
	c.Data(http.StatusOK, "application/json", cliResp.JSONBytes())

}

// UserInfoHandler ： 查询用户信息
func UserInfoHandler(c *gin.Context) {
	// 1. 解析请求参数
	var requserinfo service.ReqUserInfo
	//PanicIfUserError(c.Bind(&requserinfo))
	username := c.Request.FormValue("username")
	requserinfo = service.ReqUserInfo{
		Username: username,
	}
	userService := c.Keys["user"].(service.UserServiceClient)
	resp, err := userService.UserInfo(context.TODO(), &requserinfo)

	fmt.Println("UserInfoHandler:", resp)
	if err != nil {
		log.Println(err.Error())
		c.Status(http.StatusInternalServerError)
		return
	}

	// 3. 组装并且响应用户数据
	cliResp := res.RespMsg{
		Code: 0,
		Msg:  "OK",
		Data: gin.H{
			"Username": requserinfo.Username,
			"SignupAt": resp.SignupAt,
			// TODO: 完善其他字段信息
			"LastActive": resp.LastActiveAt,
		},
	}
	c.Data(http.StatusOK, "application/json", cliResp.JSONBytes())
}
