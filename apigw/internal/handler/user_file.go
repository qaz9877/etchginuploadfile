package handler

import (
	"apigw/internal/service"
	"apigw/pkg/util"
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// FileQueryHandler : 查询批量的文件元信息
func FileQueryHandler(c *gin.Context) {
	var requserfile service.ReqUserFile
	//PanicIfUserError(c.Bind(&requserfile))
	username := c.Request.FormValue("username")
	limitCnt, _ := strconv.Atoi(c.Request.FormValue("limit"))
	requserfile = service.ReqUserFile{
		Username: username,
		Limit:    int32(limitCnt),
	}
	userService := c.Keys["user"].(service.UserServiceClient)

	resp, err := userService.UserFiles(context.Background(), &requserfile)

	fmt.Println("FileQueryHandler", resp)
	PanicIfUserError(err)

	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.Data(http.StatusOK, "application/json", resp.FileData)
}

// FileMetaUpdateHandler ： 更新元信息接口(重命名)
func FileMetaUpdateHandler(c *gin.Context) {
	var requserfilerename service.ReqUserFileRename
	var requserfile service.ReqUserFile
	opType := c.Request.FormValue("op")
	fileSha1 := c.Request.FormValue("filehash")
	username := c.Request.FormValue("username")
	newFileName := c.Request.FormValue("filename")
	//fmt.Println("FileMetaUpdateHandler", username)
	requserfilerename = service.ReqUserFileRename{
		Username:    username,
		Filehash:    fileSha1,
		NewFileName: newFileName,
	}
	PanicIfUserError(c.Bind(&requserfilerename))
	if opType != "0" {
		c.Status(http.StatusForbidden)
		return
	}
	userService := c.Keys["user"].(service.UserServiceClient)
	_, err := userService.UserFileRename(context.Background(), &requserfilerename)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	requserfile = service.ReqUserFile{
		Username: requserfilerename.Username,
		Limit:    int32(10),
	}
	//fmt.Println("FileMetaUpdateHandler", requserfilerename.Username)
	resp1, err := userService.UserFiles(context.Background(), &requserfile)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}

	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	c.Data(http.StatusOK, "application/json", resp1.FileData)
}

func DoUploadHandler(c *gin.Context) {
	var requploadfileinfo service.ReqUploadFileInfo
	var resp1 *service.RespUploadFileInfo
	file, head, err := c.Request.FormFile("file")
	defer func() {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		if resp1.Code < 0 {
			c.JSON(http.StatusOK, gin.H{
				"code": resp1.Code,
				"msg":  "上传失败",
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"code": resp1.Code,
				"msg":  "上传成功",
			})
		}
	}()

	if err != nil {
		log.Printf("Failed to get form data, err:%s\n", err.Error())
		resp1.Code = -6
		return
	}
	defer file.Close()
	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		log.Printf("Failed to get file data, err:%s\n", err.Error())
		resp1.Code = -6
		return
	}

	requploadfileinfo = service.ReqUploadFileInfo{
		Filename: head.Filename,
		Filesha1: util.Sha1(buf.Bytes()), //　计算文件sha1
		Filesize: int32(len(buf.Bytes())),
		Uploadat: time.Now().Format("2006-01-02 15:04:05"),
		FileData: buf.Bytes(),
		Username: c.Request.FormValue("username"),
	}
	fmt.Println("DoUploadHandler", requploadfileinfo.Username)
	userService := c.Keys["user"].(service.UserServiceClient)
	resp1, err = userService.DoUploadHandler(context.Background(), &requploadfileinfo)
	if err != nil {
		resp1.Code = -6
	}

}
