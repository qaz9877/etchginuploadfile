package config

import (
	"os"

	"github.com/spf13/viper"
)

func InitConfig() {
	worker, _ := os.Getwd()
	viper.SetConfigName("config")
	viper.SetConfigFile("yml")
	viper.AddConfigPath(worker + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

}
