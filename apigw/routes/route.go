package routes

import (
	"apigw/internal/handler"
	"apigw/middleware"

	"github.com/gin-gonic/gin"
)

func NewRouter(service ...interface{}) *gin.Engine {
	ginRoute := gin.Default()
	ginRoute.Use(middleware.InitMiddleware(service))
	ginRoute.Static("/static/", "./static")
	ginRoute.GET("/user/signup", handler.SignupHandler)

	ginRoute.POST("/user/signup", handler.DoSignupHandler)

	ginRoute.GET("user/signin", handler.SigninHandler)
	ginRoute.POST("user/signin", handler.DoSigninHandler)

	//

	ginRoute.POST("/user/info", handler.UserInfoHandler)

	// 用户文件查询
	ginRoute.POST("/file/query", handler.FileQueryHandler)

	// 用户文件修改(重命名)
	ginRoute.POST("/file/update", handler.FileMetaUpdateHandler)

	// // 用户文件下载 文件
	// ginRoute.POST("/file/downloadurl", handler.FileMetadownloadHandler)

	// //文件上传
	ginRoute.POST("/file/upload", handler.DoUploadHandler)

	// // 秒传接口
	// ginRoute.POST("/file/fastupload", handler.TryFastUploadHandler)
	v1 := ginRoute.Group("/api/v1")
	{
		v1.GET("ping", func(context *gin.Context) {
			context.JSON(200, "success")
		})

	}
	return ginRoute
}
