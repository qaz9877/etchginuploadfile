package main

import (
	"account/config"
	"account/discovery"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
)

func main() {
	config.InitConfig()
	etchaddress := []string{viper.GetString("etcd.address")}
	etchRegister := discovery.NewRegister(etchaddress, logrus.New())
	grpcAddress := viper.GetString("server.grpcAddress")
	defer etchRegister.Stop()
	userNode := discovery.Server{
		Name: viper.GetString("server.domain"), //user
		Addr: grpcAddress,
	}
	server := grpc.NewServer()
	defer server.Stop()
	//绑定service
	service.RegisterUserServiceServer(server, handler.NewUserService())
	lis, err := net.Listen("tcp", grpcAddress)
	if err != nil {
		panic(err)
	}
	if _, err := etchRegister.Register(userNode, 10); err != nil {
		panic(fmt.Sprintf("start server failed, err: %v", err))
	}
	logrus.Info("server started listen on ", grpcAddress)
	if err := server.Serve(lis); err != nil {
		panic(err)
	}

}
