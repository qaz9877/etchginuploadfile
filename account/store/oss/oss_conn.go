package oss

import (
	"fmt"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/spf13/viper"
)

var ossCli *oss.Client

// Client : 创建oss client对象
func Client() *oss.Client {
	if ossCli != nil {
		return ossCli
	}

	ossCli, err := oss.New(viper.GetString("oss.OSSEndpoint"),
		viper.GetString("oss.OSSAccesskeyID"), viper.GetString("oss.OSSAccessKeySecret"))
	if err != nil {
		//	fmt.Println("四点三十分")
		fmt.Println(err.Error())
		return nil
	}
	return ossCli
}

// Bucket : 获取bucket存储空间
func Bucket() *oss.Bucket {
	fmt.Println("启动oss ")
	cli := Client()
	if cli != nil {

		bucket, err := cli.Bucket(viper.GetString("oss.OSSBucket"))
		if err != nil {
			//fmt.Println("四点三十分222")
			fmt.Println(err.Error())
			return nil
		}
		//	fmt.Println("四点三十分3333")
		return bucket
	}
	return nil
}

// DownloadURL : 临时授权下载url
func DownloadURL(objName string) string {
	signedURL, err := Bucket().SignURL(objName, oss.HTTPGet, 3600)
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return signedURL
}

// BuildLifecycleRule : 针对指定bucket设置生命周期规则
func BuildLifecycleRule(bucketName string) {
	// 表示前缀为test的对象(文件)距最后修改时间30天后过期。
	ruleTest1 := oss.BuildLifecycleRuleByDays("rule1", "test/", true, 30)
	rules := []oss.LifecycleRule{ruleTest1}

	Client().SetBucketLifecycle(bucketName, rules)
}

// GenFileMeta :  构造文件元信息
func GenFileMeta(metas map[string]string) []oss.Option {
	options := []oss.Option{}
	for k, v := range metas {
		options = append(options, oss.Meta(k, v))
	}
	return options
}
