package main

import (
	"context"
	"fmt"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

var (
	config         clientv3.Config
	leaseID        clientv3.LeaseID
	ctx            context.Context
	canceFunc      context.CancelFunc
	txn            clientv3.Txn
	client         *clientv3.Client
	LeaseGrantResp *clientv3.LeaseGrantResponse
	keepResp       *clientv3.LeaseKeepAliveResponse
	txnResp        *clientv3.TxnResponse
	keepRespChan   <-chan *clientv3.LeaseKeepAliveResponse // 只读管道
	kv             clientv3.KV
	err            error
)

/*
	lease实现锁自动过期
	op操着
	txn事务：if else then
*/

func main() {
	// 连接客户端配置文件
	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second,
	}

	// 建立连接
	if client, err = clientv3.New(config); err != nil {
		fmt.Printf("conect to etcd faild, err:%v\n", err)
		return
	} else {
		fmt.Println("connect to etcd success")
	}

	// 1、上锁(创建租约、自动续租、拿着租约去抢占一个key)
	// 申请一个租约 lease
	lease := clientv3.Lease(client)

	// 申请一个5s的租约
	if LeaseGrantResp, err = lease.Grant(context.TODO(), 5); err != nil {
		fmt.Println("租约申请失败", err)
		return
	}
	//fmt.Println("zhe3")
	// 租约ID
	leaseID = LeaseGrantResp.ID

	// 准备一个用于取消的自动续租的context; cancanceFunc 取消续租调用这个函数即可
	ctx, canceFunc = context.WithCancel(context.TODO())

	// 确保函数退出后，自动续约会停止
	defer canceFunc()
	defer lease.Revoke(context.TODO(), leaseID)

	// 自动续租
	if keepRespChan, err = lease.KeepAlive(ctx, leaseID); err != nil {
		fmt.Println("自动续租失败", err)
		return
	}

	// 判断续约应答的协程
	go func() {
		for {
			select {
			case keepResp = <-keepRespChan:
				if keepRespChan == nil {
					fmt.Println("租约已经失效了")
					goto END
				} else {
					// KeepAlive每秒会续租一次,所以就会收到一次应答
					fmt.Println("收到应答,租约ID是:", keepResp.ID)
				}
			}
		}
	END:
	}()

	// ***拿着租约去抢占一个key***
	// 获取kv API子集
	kv = clientv3.NewKV(client)

	// 创建事务
	txn = kv.Txn(context.TODO())
	//fmt.Println("zhe")
	// 定义事务
	// 如果key不存在;关联用的是clientv3.WithLease(leaseID)
	txn.If(clientv3.Compare(clientv3.CreateRevision("/cron/lock/job19"), "=", 0)).
		// 不存在就put一个key
		Then(clientv3.OpPut("/cron/lock/job19", "xxx", clientv3.WithLease(leaseID))).
		// 否则枪锁失败
		Else(clientv3.OpGet("/cron/lock/job19"))

	// 提交事务
	if txnResp, err = txn.Commit(); err != nil {
		fmt.Println("txn err", err)
		return
	}
	//fmt.Println("zhe2")
	// 判断释放抢到锁
	if !txnResp.Succeeded {
		fmt.Println("锁被占用", string(txnResp.Responses[0].GetResponseRange().Kvs[0].Value))
		return
	}

	// 2、处理业务
	fmt.Println("处理任务")
	time.Sleep(50 * time.Second)

	// 3、释放锁(取消自动续租、释放租约)
	/*
		defer canceFunc()
		defer lease.Revoke(context.TODO(), leaseID)
		上面这个释放了租约,关联的kv会被删除,从而达到释放锁
	*/
}
