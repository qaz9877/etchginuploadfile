package e

const (
	// TempLocalRootDir : 本地临时存储地址的路径
	TempLocalRootDir = "D:\\data\\fileserver\\"
	// TempPartRootDir : 分块文件在本地临时存储地址的路径
	TempPartRootDir = "D:\\data\\fileserver_part\\"
	// CephRootDir : Ceph的存储路径prefix
	CephRootDir = "/ceph"
	// OSSRootDir : OSS的存储路径prefix
	OSSRootDir = "oss/"
	// CurrentStoreType : 设置当前文件的存储类型
	CurrentStoreType = StoreOSS

	//C:/Users/ASUS/Desktop/
	LocalPartDir = "C:/Users/ASUS/Desktop/"
)
