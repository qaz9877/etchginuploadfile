package handler

import (
	"account/internal/repository"
	dbcli "account/internal/repository"
	proto "account/internal/service"
	"account/mq"
	"account/pkg/e"
	"account/pkg/util"
	"account/store/ceph"
	"account/store/oss"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
	"time"

	"github.com/spf13/viper"
)

// message ReqFastUploadFileInfo{
// 	//  @inject_tag: json:"username" form:"username" uri:"username"
// string username
//   //  @inject_tag: json:"filename" form:"filename" uri:"filename"
// string filename
//   //  @inject_tag: json:"filesha1" form:"filesha1" uri:"filesha1"
// string  filesha1
//   //  @inject_tag: json:"fileData" form:"fileData" uri:"fileData"
// bytes fileData
// }

// message RespFastUploadFileInfo {
// int32 code = 1;
// string message =2;
// bytes fileData = 3;
// }

// User : 用于实现UserServiceHandler接口的对象
type UserService struct{}

func NewUserService() *UserService {
	return &UserService{}
}

// GenToken : 生成token
func GenToken(username string) string {
	// 40位字符:md5(username+timestamp+token_salt)+timestamp[:8]
	ts := fmt.Sprintf("%x", time.Now().Unix())
	tokenPrefix := util.MD5([]byte(username + ts + "_tokensalt"))
	return tokenPrefix + ts[:8]
}

// Signup : 处理用户注册请求
func (u *UserService) Signup(ctx context.Context, req *proto.ReqSignup) (res *proto.RespSignup, err error) {
	res = new(proto.RespSignup)
	username := req.Username
	passwd := req.Password
	fmt.Println("Signup:", username, passwd)
	// 参数简单校验
	if len(username) < 3 || len(passwd) < 5 {
		res.Code = e.StatusParamInvalid
		res.Message = "注册参数无效"
		return res, errors.New("注册参数无效")
	}

	// 对密码进行加盐及取Sha1值加密
	encPasswd := util.Sha1([]byte(passwd + e.PasswordSalt))
	// 将用户信息注册到用户表中
	dbResp := dbcli.UserSignup(username, encPasswd)
	if dbResp.Suc {
		res.Code = e.StatusOK
		res.Message = "注册成功"
	} else {
		res.Code = e.StatusRegisterFailed
		res.Message = "注册失败"
	}
	return res, nil
}

// Signin : 处理用户登录请求
func (u *UserService) Signin(ctx context.Context, req *proto.ReqSignin) (res *proto.RespSignin, err error) {
	res = new(proto.RespSignin)
	username := req.Username
	password := req.Password

	encPasswd := util.Sha1([]byte(password + e.PasswordSalt))

	// 1. 校验用户名及密码
	dbResp := dbcli.UserSignin(username, encPasswd)
	if !dbResp.Suc {
		res.Code = e.StatusLoginFailed
		return res, errors.New("登入错误")
	}

	// 2. 生成访问凭证(token)
	token := GenToken(username)
	//TODO  放入redis
	rConn := repository.RedisPool().Get()

	_, err1 := rConn.Do("set", username, token, "EX", "50000")
	if err1 != nil {
		fmt.Println("添加成功", err)
	}

	// _, err = redis.String(rConn.Do("get", username))
	// if err != nil {
	// 	fmt.Println("查询失败", err)
	// }
	// upRes := dbcli.UpdateToken(username, token)
	// if !upRes.Suc {
	// 	res.Code = e.StatusServerError
	// 	return res, errors.New("token生成错误")
	// }

	// 3. 登录成功, 返回token
	res.Code = e.StatusOK
	res.Token = token
	return res, nil
}

// UserInfo ： 查询用户信息
func (u *UserService) UserInfo(ctx context.Context, req *proto.ReqUserInfo) (res *proto.RespUserInfo, err error) {
	res = new(proto.RespUserInfo)
	// 查询用户信息
	dbResp := dbcli.GetUserInfo(req.Username)
	// 查不到对应的用户信息
	if !dbResp.Suc {
		res.Code = e.StatusUserNotExists
		res.Message = "用户不存在"
		return res, errors.New("查询错误")
	}

	user := dbcli.ToTableUser(dbResp.Data)

	// 3. 组装并且响应用户数据
	res.Code = e.StatusOK
	res.Username = user.UserName
	res.SignupAt = user.SignupAt.Format("")
	res.LastActiveAt = user.LastActive.Format("")
	res.Status = int32(user.Status)
	// TODO: 需增加接口支持完善用户信息(email/phone等)
	res.Email = user.Email
	res.Phone = user.Phone
	return res, nil
}

// 获取用户文件
func (u *UserService) UserFiles(ctx context.Context, req *proto.ReqUserFile) (res *proto.RespUserFile, err error) {
	res = new(proto.RespUserFile)
	dbResp := dbcli.QueryUserFileMetas(req.Username, int64(req.Limit))
	if !dbResp.Suc {
		res.Code = e.StatusUserNotExists
		res.Message = "查询失败"
		return res, errors.New("查询失败")
	}

	userFiles := dbcli.ToTableUserFiles(dbResp.Data)
	data, err := json.Marshal(userFiles)
	if err != nil {
		res.Code = e.StatusServerError
		return res, err
	}

	res.FileData = data
	return res, nil
}

// 获取用户文件
func (u *UserService) UserFileRename(ctx context.Context, req *proto.ReqUserFileRename) (res *proto.RespUserFileRename, err error) {
	//res = new(proto.RespUserFile)

	res = new(proto.RespUserFileRename)
	dbResp := dbcli.RenameFileName(req.Username, req.Filehash, req.NewFileName)
	if !dbResp.Suc {
		res.Code = e.StatusUserNotExists
		res.Message = "查询失败"
		return res, errors.New("查询失败")
	}
	userFiles := dbcli.ToTableUserFiles(dbResp.Data)
	data, err := json.Marshal(userFiles)
	if err != nil {
		res.Code = e.StatusServerError
		return res, err
	}

	res.FileData = data
	return res, nil
}

func (u *UserService) DoUploadHandler(ctx context.Context, req *proto.ReqUploadFileInfo) (res *proto.RespUploadFileInfo, err error) {
	// 4. 将文件写入临时存储位置
	res = new(proto.RespUploadFileInfo)
	Location := e.TempLocalRootDir + req.Filesha1 // 临时存储地址

	err = os.MkdirAll(path.Dir(Location), 0744)
	if err != nil {
		fmt.Println(err)
		res.Code = -3
		return
	}

	newFile, err := os.Create(Location)
	if err != nil {
		log.Printf("Failed to create file, err:%s\n", err.Error())
		res.Code = -3
		return
	}

	defer newFile.Close()
	nByte, err := newFile.Write(req.FileData)
	if int64(nByte) != int64(req.Filesize) || err != nil {
		log.Printf("Failed to save data into file, writtenSize:%d, err:%s\n", nByte, err.Error())
		res.Code = -4
		return
	}
	newFile.Seek(0, 0)
	if e.CurrentStoreType == e.StoreCeph {
		//文件写入cephC存储
		data, _ := ioutil.ReadAll(newFile)
		cephPath := e.CephRootDir + req.Filesha1
		_ = ceph.PutObject("userfile", cephPath, data)
		Location = cephPath

	} else if e.CurrentStoreType == e.StoreOSS {

		// 文件写入OSS存储
		ossPath := e.OSSRootDir + req.Filesha1

		if true {

			// TODO: 设置oss中的文件名，方便指定文件名下载
			err = oss.Bucket().PutObject(ossPath, newFile)
			if err != nil {
				log.Println(err.Error())
				res.Code = -5
				return
			}
			Location = ossPath
		} else {
			// 写入异步转移任务队列

			data := mq.TransferData{
				FileHash:      req.Filesha1,
				CurLocation:   Location,
				DestLocation:  ossPath,
				DestStoreType: e.StoreOSS,
			}
			pubData, _ := json.Marshal(data)
			pubSuc := mq.Publish(

				viper.GetString("Rabbit.TransExchangeName"),
				viper.GetString("Rabbit.TransOSSRoutingKey"),
				pubData,
			)
			if !pubSuc {
				// TODO: 当前发送转移信息失败，稍后重试
			}

		}

	}
	//6.  更新文件表记录strconv.FormatInt(int64,10)
	dbResp := dbcli.OnFileUploadFinished(req.Filesha1, req.Filename, int64(req.Filesize), Location)
	if !dbResp.Suc {
		res.Code = -6
		return
	}

	// 7. 更新用户文件表

	dbResp2 := dbcli.OnUserFileUploadFinished(req.Username, req.Filesha1, req.Filename, int64(req.Filesize))
	if dbResp2.Suc {
		res.Code = 0
	} else {
		res.Code = -6
	}
	return
}

func (u *UserService) FileMetadownloadHandler(ctx context.Context, req *proto.ReqdownFile) (res *proto.RespdownFileInfo, err error) {
	res = new(proto.RespdownFileInfo)
	dbResp := dbcli.GetFileMeta(req.Filesha1)
	if !dbResp.Suc {
		res.Code = e.StatusUserNotExists
		res.Message = "查询失败"
		return res, errors.New("查询失败")
	}
	file := dbcli.ToTableFlie(dbResp.Data)
	dbResp2 := dbcli.QueryUserFileMeta(file.FileName, file.FileSha1)
	if !dbResp2.Suc {
		res.Code = e.StatusUserNotExists
		res.Message = "查询失败"
		return res, errors.New("查询失败")
	}
	userfile := dbcli.ToTableUserFlie(dbResp2.Data)
	fpath := e.LocalPartDir + userfile.FileName
	os.MkdirAll(path.Dir(fpath), 0744)

	fmt.Println(file.FileAddr)
	if strings.HasPrefix(file.FileAddr, "oss/") {
		err := oss.Bucket().GetObjectToFile(file.FileAddr, e.LocalPartDir+userfile.FileName)
		if err != nil {

			fmt.Println(userfile.FileName)

			return res, err
		}
	} else {
		fmt.Println(userfile.FileName)
		copy(e.TempLocalRootDir+file.FileSha1, e.LocalPartDir+userfile.FileName)
	}
	return res, nil
}

func copy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}
