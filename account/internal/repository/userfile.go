package repository

import (
	"fmt"
	"time"

	"account/pkg/res"
	"account/pkg/util"

	"github.com/mitchellh/mapstructure"
)

type TblUserFile struct {
	ID         int `gorm:"primarykey"`
	UserName   string
	FileSha1   string
	FileSize   int
	FileName   string
	UploadAt   time.Time
	LastUpload time.Time
	Status     int
}

// QueryUserFileMetas : 批量获取用户文件信息
func QueryUserFileMetas(username string, limit int64) (exre res.ExecResult) {

	var userfiles []TblUserFile
	if err := DB.Where("user_name=?", username).Find(&userfiles).Limit(int(limit)).Error; err != nil {
		exre.Suc = false
		exre.Data = userfiles
		return

	}

	exre.Suc = true
	exre.Data = userfiles
	return
}

func QueryUserFileMeta(username string, filesha1 string) (exre res.ExecResult) {

	var userfiles TblUserFile
	if err := DB.Where("user_name=? and file_sha1", username, filesha1).First(&userfiles).Error; err != nil {
		exre.Suc = false
		exre.Data = userfiles
		return

	}

	exre.Suc = true
	exre.Data = userfiles
	return
}

func OnUserFileUploadFinished(username, filehash, filename string, filesize int64) (exre res.ExecResult) {
	var tblflie TblUserFile
	tblflie = TblUserFile{
		UserName: username,
		FileSha1: filehash,
		FileName: filename,
		FileSize: int(filesize),
	}
	if err := DB.Select(`user_name`, `file_sha1`, `file_name`, `file_size`).Create(&tblflie).Error; err != nil {
		util.LogrusObj.Error("Insert User Error:" + err.Error())
		exre.Suc = false
		return
	}
	exre.Suc = true
	return
}

// RenameFileName : 文件重命名
func RenameFileName(username, filehash, filename string) (exre res.ExecResult) {
	var userfile TblUserFile
	if err := DB.Model(&userfile).Where(map[string]interface{}{"file_sha1=?": filehash, "user_name": username}).Update("file_name", filename).Error; err != nil {
		exre.Suc = false
		exre.Data = userfile
		return

	}
	fmt.Println(userfile)
	//TODO   查询更新子厚的信息

	exre.Suc = true
	exre.Data = userfile
	return
}

func ToTableUserFiles(src interface{}) []TblUserFile {
	ufile := []TblUserFile{}
	mapstructure.Decode(src, &ufile)
	return ufile
}

func ToTableUserFlie(src interface{}) TblUserFile {
	file := TblUserFile{}
	mapstructure.Decode(src, &file)
	return file
}
