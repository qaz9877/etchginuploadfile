package repository

import (
	"account/pkg/util"
	"os"
)

func migration() {
	//自动迁移
	err := DB.Set("gorm:table_options", "charset=utf8mb4").AutoMigrate(
		&TblUser{}, &TblUserToken{}, &TblFile{}, &TblUserFile{},
	)
	if err != nil {
		util.LogrusObj.Infoln("register table fail")
		os.Exit(0)
	}
	util.LogrusObj.Infoln("register table success")

}
