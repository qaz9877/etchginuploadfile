package repository

import (
	"account/pkg/res"
	"account/pkg/util"
	"time"

	"github.com/mitchellh/mapstructure"
	"gorm.io/gorm"
)

type TblUser struct {
	ID             uint   `gorm:"primarykey"`
	UserName       string `gorm:"unique"`
	UserPwd        string
	Email          string
	Phone          string
	EmailValidated int
	PhoneValidated int
	SignupAt       time.Time
	LastActive     time.Time
	Profile        string
	Status         int
}

const (
	PassWordCost = 12 // 密码加密难度
)

// UserSignup : 通过用户名及密码完成user表的注册操作
func UserSignup(username string, passwd string) (exre res.ExecResult) {
	var user TblUser
	var count int64
	DB.Table("tbl_user").Where("user_name=?", username).Count(&count)
	if count != 0 {
		util.LogrusObj.Error("Failed to insert, err:")

		exre.Suc = false
		return
	}
	user = TblUser{
		UserName: username,
		UserPwd:  passwd,
	}
	if err := DB.Select("user_name", "user_pwd").Create(&user).Error; err != nil {
		util.LogrusObj.Error("Insert User Error:" + err.Error())
		exre.Suc = false
		return
	}
	exre.Suc = true
	return
}

// UserSignin : 判断密码是否一致
func UserSignin(username string, encpwd string) (exre res.ExecResult) {
	var user TblUser
	if err := DB.Where("user_name=?", username).First(&user).Error; err == gorm.ErrRecordNotFound {
		exre.Suc = false

	}
	if user.UserPwd != encpwd {
		exre.Suc = false
		return
	}
	exre.Suc = true
	return
}

// GetUserInfo : 查询用户信息
func GetUserInfo(username string) (exre res.ExecResult) {
	var user TblUser
	if err := DB.Where("user_name=?", username).First(&user).Error; err == gorm.ErrRecordNotFound {
		exre.Suc = false
		exre.Msg = "Failed to GetUserInfo"
		util.LogrusObj.Error("Failed to GetUserInfo, err:")
		return
	}
	if user.UserPwd == "" {
		exre.Suc = false
		exre.Msg = "Failed to GetUserInfo"
		util.LogrusObj.Error("Failed to GetUserInfo, err:")
		return
	}
	exre.Suc = true
	exre.Msg = "success to GetUserInfo"
	exre.Data = user
	return

}
func (user *TblUser) CheckUserExist(username string) bool {
	if err := DB.Where("user_name=?", username).First(&user).Error; err == gorm.ErrRecordNotFound {
		return false
	}
	return true
}

// UserExist : 查询用户是否存在
func UserExist(username string) (exre res.ExecResult) {
	var user TblUser
	if exist := user.CheckUserExist(username); exist {
		exre.Suc = false
		util.LogrusObj.Error("Failed to UserExist, err:")
		return
	}
	exre.Suc = true
	return
}

func ToTableUser(src interface{}) TblUser {
	file := TblUser{}
	mapstructure.Decode(src, &file)
	return file
}
