package repository

import (
	"account/pkg/res"
	"account/pkg/util"
	"time"

	"github.com/mitchellh/mapstructure"
	"gorm.io/gorm"
)

type TblFile struct {
	ID       int `gorm:"primarykey"`
	FileSha1 string
	FileName string
	FileSize int64
	FileAddr string
	CreateAt time.Time
	UpdateAt time.Time
	Status   int
	Ext1     int
	Ext2     string
}

func OnFileUploadFinished(filehash string, filename string, filesize int64, fileaddr string) (exre res.ExecResult) {
	var tblflie TblFile
	tblflie = TblFile{
		FileSha1: filehash,
		FileName: filename,
		FileSize: filesize,
		FileAddr: fileaddr,
	}
	if err := DB.Select(`file_sha1`, `file_name`, `file_size`, `file_addr`, `status`).Create(&tblflie).Error; err != nil {
		util.LogrusObj.Error("Insert User Error:" + err.Error())
		exre.Suc = false
		return
	}
	exre.Suc = true
	return
}

func GetFileMeta(filehash string) (exre res.ExecResult) {
	tfile := TblFile{}

	if err := DB.Where("file_sha1=?", filehash).First(&tfile).Error; err == gorm.ErrRecordNotFound {
		exre.Suc = false
		exre.Msg = "Failed to GetUserInfo"
		util.LogrusObj.Error("Failed to GetUserInfo, err:")
		return
	}
	if tfile.FileSha1 == "" {
		exre.Suc = false
		exre.Msg = "Failed to GetUserInfo"
		util.LogrusObj.Error("Failed to GetUserInfo, err:")
		return
	}
	exre.Suc = true
	exre.Msg = "success to GetUserInfo"
	exre.Data = tfile
	return
}

func ToTableFlie(src interface{}) TblFile {
	file := TblFile{}
	mapstructure.Decode(src, &file)
	return file
}
