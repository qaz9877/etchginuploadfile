package repository

import (
	"account/pkg/res"
)

type TblUserToken struct {
	ID        int `gorm:"primarykey"`
	UserName  string
	UserToken string
}

// UpdateToken : 刷新用户登录的token
func UpdateToken(username string, token string) (exre res.ExecResult) {
	var usertoken TblUserToken
	var count int64
	DB.Table("tbl_user_token").Where("user_name=?", username).Count(&count)
	if count == 0 {

		usertoken = TblUserToken{
			UserName:  username,
			UserToken: token,
		}
		results := DB.Select("user_name", "user_token").Create(&usertoken)
		if results.Error != nil {
			exre.Suc = false

			return
		} else {
			exre.Suc = true

			return
		}

	} else {
		if err := DB.Model(&usertoken).Where("user_name=?", username).Update("user_token", token).Error; err != nil {
			exre.Suc = false
			exre.Msg = "Failed to UpdateToken"
			return
		}
		exre.Suc = true

		return
	}

}
